#!/bin/bash

set -e

cd $(dirname $0)

kvm \
	-machine q35 -smp cpus=4,cores=2,threads=2,sockets=1 -m 4096 \
	-rtc base=localtime \
	-vga std -usb -device usb-tablet -soundhw hda -device hda-output -device ahci,id=ahci \
	-drive id=disk,file=win10.img,if=none,cache=none \
	-device ide-drive,drive=disk,bus=ahci.0 \
	-net nic -net user,hostfwd=tcp::7722-:22
